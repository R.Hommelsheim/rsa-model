package stage

import actors.Agent
import actors.general.GeneralAgent
import actors.personal.PersonalAgent
import pragmatic_updating.Metrics
import props.{Lexicon, Referent, Signal}

case class Monologue[A <: Agent](override val agent1: A,
                                 override val agent2: A,
                                 override val signals: Vector[Signal],
                                 override val referents: Vector[Referent],
                                 override val sameIntention: Boolean,
                                 override val n: Int,
                                 override val m: Int,
                                 override val order: Int,
                                 override val convLength: Int,
                                 override val nrOfConv: Int)
  extends Conversation(agent1, agent2, signals, referents, sameIntention, n, m, order, convLength, nrOfConv) {

  override def turn(intention: Referent,
                    sigRef: Vector[(Signal, Referent)]
                   ): Vector[(Signal, Referent)] = {

    // agent 1 creates a signal
    val speaker = agent1.asSpeaker
    val signal = speaker.speak(intention)

    // agent 2 infers a referent
    val listener = agent2.asListener
    val referent = listener.listen(signal)

    (agent1, agent2) match {
      case (_: GeneralAgent, _: GeneralAgent) =>
        setSigRef(sigRef, signal, referent)

      case (_: PersonalAgent, _: PersonalAgent) =>
        // saving states
        agent1.myIntention = speaker.myIntention
        agent2.rPrior = listener.rPrior
        setSigRef(sigRef, signal, referent)

      case _ =>
        println("NOTHING MATCHED")
        sigRef
    }
  }

  def setSigRef(sigRef: Vector[(Signal, Referent)],
                signal: Signal,
                referent: Referent):
  Vector[(Signal, Referent)] = {
    val newSigRef = sigRef.union(Vector((signal, referent)))
    // Set the agents personal lists
    agent1.setSigRef(newSigRef)
    agent2.setSigRef(newSigRef)
    newSigRef
  }

  /**
   *
   * @param allIntentions All intentions of a conversation
   * @param conversations All conversations
   * @return Vector of Vectors which holds booleans of whether intention equals referent
   */
  override def evaluate(
                         allIntentions: Vector[Vector[Referent]],
                         conversations: Vector[Vector[(Signal, Referent)]]):
  Vector[Vector[Int]] =
  // This is zipping each intention with the inferred referent by the second agent
    allIntentions zip
      conversations map { case (allI, convs) => allI zip convs } map {
      _ map { case (i, (_, r)) => if (i == r) 1 else 0}
    }
}
