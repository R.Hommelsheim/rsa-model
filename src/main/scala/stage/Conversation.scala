package stage

import actors.Agent
import props.{Lexicon, Referent, Signal}

import scala.util.Random

/**
 *
 * @param n          : number of signals
 * @param m          : number of referents
 * @param order      : order of pragmatic inference
 * @param convLength : nr of interactions (the total number of rounds is one conversation)
 * @param nrOfConv   : nr of runs (the total number of runs includes all conversations)
 */
abstract class Conversation[A <: Agent](val agent1: A,
                                        val agent2: A,
                                        val signals: Vector[Signal],
                                        val referents: Vector[Referent],
                                        val sameIntention: Boolean,
                                        val n: Int = 3,
                                        val m: Int = 3,
                                        val order: Int = 2,
                                        val convLength: Int = 10,
                                        val nrOfConv: Int = 1) {


  // get all intentions, either each vector has the same intention or each vector is filled with different intentions
  val allIntentions: Vector[Vector[Referent]] =
    if (sameIntention) {
      val getInt: Int => Vector[Referent] = (ind: Int) => Vector.fill(convLength)(referents(ind))
      Vector.fill(nrOfConv)(getInt(Random.nextInt(m)))
    } else
      Vector.fill(nrOfConv)(Vector.fill(convLength)(Random.shuffle(referents).take(1)(0)))

  // One turn: Implemented by the conversation type
  def turn(intention: Referent,
           sigRef: Vector[(Signal, Referent)]):
  Vector[(Signal, Referent)]

  def converse(sigRef: Vector[(Signal, Referent)],
               intentions: Vector[Referent],
               convCount: Int):
  Vector[(Signal, Referent)] = convCount match {
    case this.convLength => sigRef
    case _ if sigRef.isEmpty =>
      agent1.setSigRef(sigRef)
      agent2.setSigRef(sigRef)
      val seed = Random.nextInt()
      agent1.lexicon = Lexicon(signals, referents, seed)
      agent2.lexicon = Lexicon(signals, referents, seed)
      agent1.rPrior = agent1.equalPrior()
      agent2.rPrior = agent2.equalPrior()
      converse(turn(intentions(convCount), sigRef), intentions, convCount + 1)
    case _ => converse(turn(intentions(convCount), sigRef), intentions, convCount + 1)
  }

  def run(): Vector[Vector[(Signal, Referent)]] =
    for (intentions <- allIntentions) yield
      converse(Vector(), intentions, 0)

  def interact(): Vector[Vector[Int]] = evaluate(allIntentions, run())

  /**
   *
   * @param allIntentions All intentions of a conversation
   * @param conversations All conversations
   * @return Vector of Vectors which holds booleans of whether intention equals referent
   */
  def evaluate(allIntentions: Vector[Vector[Referent]], conversations:
  Vector[Vector[(Signal, Referent)]]):
  Vector[Vector[Int]]

}




