package stage

import actors.Agent
import props.{Referent, Signal}

case class Dialogue[A <: Agent](override val agent1: A,
                                override val agent2: A,
                                override val signals: Vector[Signal],
                                override val referents: Vector[Referent],
                                override val sameIntention: Boolean,
                                override val n: Int,
                                override val m: Int,
                                override val order: Int,
                                override val convLength: Int,
                                override val nrOfConv: Int)
  extends Conversation(agent1, agent2, signals, referents, sameIntention, n, m, order, convLength, nrOfConv) {

  override def turn(intention: Referent,
                    sigRef: Vector[(Signal, Referent)]
                   ): Vector[(Signal, Referent)] = {

    // Essentially two monologues, the first one as usual
    val sigRef1 = Monologue.apply(
      agent1,
      agent2,
      signals,
      referents,
      sameIntention,
      n,
      m,
      order,
      convLength,
      nrOfConv)
      .turn(intention, sigRef)

    // The second with switched agent so that they will switch roles and now the
    // intention of the second agent is the inferred referent
    Monologue.apply(
      agent2,
      agent1,
      signals,
      referents,
      sameIntention,
      n,
      m,
      order,
      convLength,
      nrOfConv)
      .turn(agent2.agentSigRef.last._2, sigRef1)
  }

  /**
   *
   * @param allIntentions All intentions of a conversation
   * @param conversations All conversations
   * @return Vector of Vectors which holds booleans of successes
   */
  override def evaluate(
                         allIntentions: Vector[Vector[Referent]],
                         conversations: Vector[Vector[(Signal, Referent)]]):
  Vector[Vector[Int]] = {

    // Splitting data according to conversation
    val segmented: Vector[Vector[Vector[(Signal, Referent)]]] =
      conversations map { conv => conv.sliding(2, 2).toVector }

    // Taking out the signals
    val onlyReferents: Vector[Vector[(Referent, Referent)]] =
      segmented map { seg => seg map { case Vector((_, i), (_, j)) => (i, j) } }

    // Zipping with intentions
    val allIntRef: Vector[(Vector[Referent], Vector[(Referent, Referent)])] =
      allIntentions zip onlyReferents

    // Zipping individual intentions with ref pairs
    val intRef: Vector[Vector[(Referent, (Referent, Referent))]] =
      allIntRef map { case (ints, refs) => ints zip refs }

    // Getting the final results
    val result =
      intRef map {
        conv =>
          conv map {
            case (i, (refA2, refA1)) if i == refA2 && refA2 == refA1 => 1
            case (i, (refA2, refA1)) if i == refA2 && refA2 != refA1 => 2
            case (i, (refA2, refA1)) if i != refA2 && refA2 == refA1 => 3
            case (i, (refA2, refA1)) if i != refA2 && refA2 != refA1 && i == refA1 => 4
            case (i, (refA2, refA1)) if i != refA2 && refA2 != refA1 && i != refA1 => 5
          }
      }

    result
  }
}
