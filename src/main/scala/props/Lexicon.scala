package props

import pragmatic_updating.Metrics

import scala.collection.immutable.Queue
import scala.util.Random

case class Lexicon(L: Vector[Vector[Double]]) {

  val sig: Int = L.size
  val ref: Int = L(0).size

  override def toString: String =
    L.map(row =>
      (row map {
        case e@(0 | 1) => f"${e.toInt}   "
        case e => f"$e%.2f"
      }) mkString "\t")
      .mkString("\n")

  def rowString: String = L(0).flatMap(_ => "------").mkString

  def T: Lexicon = Lexicon(L.transpose)

  def normalize: Lexicon = Lexicon(L map Metrics.normalize)

  def copy: Lexicon = Lexicon(L)

  def ambiguity(): Double =
    (for (row <- L.transpose) yield row.sum / row.size).sum / ref
}


object Lexicon {

  def apply(signals: Vector[Signal],
            referents: Vector[Referent],
            seed: Int = 0):
  Lexicon
  = {
    val lex = randomLexicon(signals.size, referents.size, seed)
//    val lex = lexiconWithEqualAmbiguity(signals, referents)
    Lexicon(lex)
  }

  def randomLexicon(sig: Int, ref: Int, seed: Int): Vector[Vector[Double]] = {
    val r = new Random(seed)
    val refTemp = Vector.fill(Math.round(0.7 * ref).toInt)(0) ++ Vector.fill(Math.round(0.3 * ref).toInt)(1)
    val sigTemp = Vector.fill(sig)(r.shuffle(refTemp))
    val lex = Vector.tabulate(sig, ref) {
      case (i, j) if i == j => 1d
      case (i, j) => sigTemp(i)(j)
    }
    lex
  }

  private def lexiconWithEqualAmbiguity(
                                         signals: Vector[Signal],
                                         referents: Vector[Referent]
                                       ): Vector[Vector[Double]] = {

    // Sizes
    val sig: Int = signals.size
    val ref: Int = referents.size

    // Rest
    val rest: Int = sig min ref

    // Calculate max capacity from each referent to sink
    // This determines ambiguity. amb=2 is 50%, amb=4 is 25%
    val amb: Double = 3d
    val maxCap: Double = amb match {
      case 0 => (sig * ref - rest) / ref
      case _ => (sig * ref / amb - rest) / ref
    }

    // total number of vertices (incl. start and target)
    val n: Int = sig + ref + 2

    // Helper function to make a bipartite graph (I know it's a bit ugly)
    def calculateCellValue(i: Int, j: Int): Double = {
      (i, j) match {
        // First row
        case (0, j) if j > 0 && j <= sig => maxCap
        case (0, j) if j > sig => 0
        // Last column
        case (i, j) if j == ref + sig + 1 && (i <= sig || i == j) => 0
        case (i, j) if j == ref + sig + 1 && i > sig => maxCap
        // First column
        case (_, 0) => 0
        // Last row
        case (i, _) if i == sig + ref + 1 => 0
        // Left upper corner (intra-connections in bipartite graph)
        case (i, j) if i <= sig && j <= sig => 0
        // Bottom right corner (intra-connections in bipartite graph)
        case (i, j) if i > sig && j > sig => 0
        // Upper right (inter-connections in bipartite graph)
        case (i, j) if i <= sig && i + sig == j => 0
        case (i, _) if i <= sig => 1
        // Lower left (inter-connections in bipartite graph)
        case (i, j) if i > sig && i == j + sig => 0
        case (i, _) if i > sig => 1
        case _ => -1
      }
    }

    // make a bipartite graph
    val graph = Vector.tabulate(n, n)(calculateCellValue)

    // start and target nodes
    val s = 0
    val t = sig + ref + 1

    val (_, connections) = maxFlow(graph, Vector[(Int, Int)](), s, t)

    // maps nodes back from stacked to individual rows
    val cons = connections map { case (u, v) => (u - 1, v - (sig + 1)) }

    //    val splitCons = cons.splitAt(cons.size / 2)
    val splitCons = cons.drop(Random.nextInt(sig))

    val lex: Vector[Vector[Double]] = Vector.tabulate(sig, ref) {
      // Diagonal
      case (i, j) if i == j => 1d
      case (i, j) if cons.contains((i, j)) => 1d
      //      case (i, j) if splitCons._2.contains((i, j)) => 1d
      //      case (i, j) if splitCons._1.contains((j, i)) => 1d
      case _ => 0d
    }

    lex
  }

  private def maxFlow(
                       residualGraph: Vector[Vector[Double]],
                       connections: Vector[(Int, Int)],
                       s: Int,
                       t: Int,
                       flow: Double = 0):
  (Double, Vector[(Int, Int)]) = {

    // find a new path
    val path = findPathBFS(residualGraph, s, t)
    if (path.nonEmpty) {

      // find the bottleneck capacity
      val bottleneck = path.map { case (u, v) => residualGraph(u)(v) }.min

      val newResidualGraph = path.foldLeft(residualGraph) {
        case (rGraph, (a, b)) =>
          val flowForward = rGraph(a)(b)
          val flowBackward = rGraph(b)(a)
          val newGraph = update(rGraph, a, b, flowForward - bottleneck)
          update(newGraph, b, a, flowBackward + bottleneck)
      }
      val newFlow = flow + bottleneck
      maxFlow(newResidualGraph, connections :+ path(1), s, t, newFlow)
    }
    else (flow, connections)
  }

  def update(graph: Vector[Vector[Double]], u: Int, v: Int, w: Double): Vector[Vector[Double]] =
    graph.updated(u, graph(u).updated(v, w))

  private def buildPath(child: Int, parents: Map[Int, Int]): List[(Int, Int)] = {
    parents
      .get(child)
      .map(p =>
        (p, child) +: buildPath(p, parents))
      .getOrElse(Nil)
  }

  private def findPathBFS(graph: Vector[Vector[Double]], s: Int, t: Int): List[(Int, Int)] = {
    val sq = Stream.iterate((Queue(s), Set(s), Map[Int, Int]())) {
      case (q, visited, parents) =>
        val (u, us) = q.dequeue
        val nbours = graph(u)
          .zipWithIndex
          .collect { case (f, id) if f > 0 => id }.toSet -- visited
        val newQueue = us ++ nbours
        val newVisited = nbours ++ visited
        val newParents = parents ++ nbours.map(_ -> u)
        (newQueue, newVisited, newParents)
    }
    val parentsMap = sq.takeWhile(q => q._1.nonEmpty).last._3
    buildPath(t, parentsMap).reverse
  }
}
