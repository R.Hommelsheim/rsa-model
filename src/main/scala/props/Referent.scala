package props

class Referent(val id:Int) {
  override def toString: String = id.toString
}
