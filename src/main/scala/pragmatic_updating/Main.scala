package pragmatic_updating

object Main extends App {
  // TODO: Document Usage 
  Simulation(
    path = "/Users/Ron/Documents/Coding/Scala/ReMis/output/", // path to save csv in
    saveData = true,     // saveData boolean, saves data if true
    Seq(("rmh", "rmh")), // agent types as (Speaker, Listener)
    Seq("Dialogue"),    // conversationTypes Monologue, dialogue
    Seq(true),           // sameIntentions we can have the same intention over a conversation, or talk about different things
    Seq(16),              // ns number of signals
    Seq(8),              // ms number of referents
    Seq(2),              // orders of pragmatic inference
    Seq(5),              // convLengths length of a conversation
    Seq(10000)              // nrOfConvs number of conversations
  )
}
