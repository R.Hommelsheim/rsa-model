package pragmatic_updating

import actors.Agent
import props.{Referent, Signal}
import stage.{Conversation, Dialogue, Monologue}

case object ConversationFactory {
  def apply(conversationType: String,
            agent1: Agent,
            agent2: Agent,
            signals: Vector[Signal],
            referents: Vector[Referent],
            sameIntention: Boolean,
            n: Int,
            m: Int,
            order: Int,
            convLength: Int,
            nrOfConv: Int): Conversation[Agent] = conversationType match {
    case "Monologue" => Monologue(agent1, agent2, signals, referents, sameIntention, n, m, order, convLength, nrOfConv)
    case "Dialogue" => Dialogue(agent1, agent2, signals, referents, sameIntention, n, m, order, convLength, nrOfConv)
  }
}