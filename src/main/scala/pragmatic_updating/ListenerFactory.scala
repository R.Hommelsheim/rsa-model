package pragmatic_updating

import actors.Agent
import actors.general.FghListener
import actors.personal.RmhListener
import props.{Referent, Signal}

case object ListenerFactory {

  def apply(agentType: String,
            name: String,
            conversationType: String,
            order: Int,
            signals: Vector[Signal],
            referents: Vector[Referent]):
  Agent = agentType match {
    case "fgh" => FghListener(name, conversationType, order, signals, referents)
    case "rmh" => RmhListener(name, conversationType, order, signals, referents)
  }

}