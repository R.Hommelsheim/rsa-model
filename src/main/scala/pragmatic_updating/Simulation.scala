package pragmatic_updating

import java.time.LocalDateTime

import actors.Agent
import org.apache.spark.sql.SparkSession
import props.{Referent, Signal}
import stage.Conversation

/**
 *
 * @param path              path to save csv in
 * @param saveData          boolean, saves data if true
 * @param conversationTypes Monologue, dialogue
 * @param sameIntentions    we can have the same intention over a conversation, or talk about different things
 * @param ns                number of signals
 * @param ms                number of referents
 * @param orders            of pragmatic inference
 * @param convLengths       length of a conversation
 * @param nrOfConvs         number of conversations
 */
case class Simulation(
                       path: String = "",
                       saveData: Boolean = false,
                       agentPairs: Seq[(String, String)] = Seq(("fgh", "fgh")),
                       conversationTypes: Seq[String] = Seq("Dialogue"),
                       sameIntentions: Seq[Boolean] = Seq(true),
                       ns: Seq[Int] = Seq(3),
                       ms: Seq[Int] = Seq(3),
                       orders: Seq[Int] = Seq(2),
                       convLengths: Seq[Int] = Seq(10),
                       nrOfConvs: Seq[Int] = Seq(10),
                     ) {

  val data: Seq[(String, String, String, Boolean, Int, Int, Int, Int, Int, String, String, Double, String)] = for (
    agents <- agentPairs;
    convType <- conversationTypes;
    sameIntent <- sameIntentions;
    order <- orders;
    n <- ns;
    m <- ms;
    convLength <- convLengths;
    nrOfConv <- nrOfConvs
  ) yield run(agents, convType, sameIntent, n, m, order, convLength, nrOfConv)

  def run(agents: (String, String),
          conversationType: String,
          sameIntention: Boolean,
          n: Int,
          m: Int,
          order: Int,
          convLength: Int,
          nrOfConv: Int
         )
  : (String, String, String, Boolean, Int, Int, Int, Int, Int, String, String, Double, String) = {

    // Create signals and referents
    val signals: Vector[Signal] = Vector.range(0, n).map(new Signal(_))
    val referents: Vector[Referent] = Vector.range(0, m).map(new Referent(_))

    // Create agents
    val agent1: Agent = SpeakerFactory.apply(agents._1, "Plato", conversationType, order, signals, referents)
    val agent2: Agent = ListenerFactory.apply(agents._2, "Socrates", conversationType, order, signals, referents)

    // Enact a conversation
    val conversation: Conversation[Agent] = ConversationFactory.apply(
      conversationType,
      agent1,
      agent2,
      signals,
      referents,
      sameIntention,
      n,
      m,
      order,
      convLength,
      nrOfConv)

    // get the results
    val results: Vector[Vector[Int]] = conversation.interact()

    // calculate success
    val success: Seq[Double] = results map Metrics.success
//        success foreach (s => println(s"Success: $s"))

    // calculate success over time
    val success_dts: Seq[Vector[Double]] = results map Metrics.success_dt
//        success_dts foreach (s => println(s"Success over time: $s"))

    // calculate the average over all conversations
    val avg: Double = Metrics.avg(results)
//        println(s"Average: $avg")

    printInfo(agents, conversationType, sameIntention, n, m, order, convLength, nrOfConv, avg)

    (
      agents._1,
      agents._2,
      conversationType,
      sameIntention,
      n,
      m,
      order,
      convLength,
      nrOfConv,
      success.mkString(","),
      (success_dts map (_.mkString(","))).mkString(";"),
      avg,
      (results map (_.mkString(","))).mkString(";")
    )
  }

  def printInfo(agents: (String, String),
                conversationType: String,
                sameIntention: Boolean,
                n: Int,
                m: Int,
                order: Int,
                convLength: Int,
                nrOfConv: Int,
                avg: Double
               ): Unit = {
    println(s"" +
      s"+------------------------------------------+\n" +
      s"+------------------------------------------+\n" +
      s"|                                           \n" +
      s"|  Agent 1                 = ${agents._1} \n" +
      s"|  Agent 2                 = ${agents._2} \n" +
      s"|  Conversation Type       = $conversationType \n" +
      s"|  Same Intentions         = $sameIntention \n" +
      s"|  Number of Signals       = $n \n" +
      s"|  Number of Referents     = $m \n" +
      s"|  Order                   = $order \n" +
      s"|  Conversation Length     = $convLength \n" +
      s"|  Number of Conversations = $nrOfConv \n" +
      s"|  Average                 = $avg \n" +
      s"|                                           \n" +
      s"+------------------------------------------+\n" +
      s"+------------------------------------------+\n" +
      s"                                            \n" +
      s"                                            \n"
    )
  }

  if (saveData) {

    // spark session
    val spark = SparkSession
      .builder
      .appName("ReMis")
      .config("spark.master", "local")
      .getOrCreate()

    import spark.implicits._

    // dataframe and print to csv
    val df = data.toDF(
      "Agent1",
      "Agent2",
      "Conversation Type",
      "Same Intention",
      "Signals",
      "Referents",
      "Order",
      "Conversation Length",
      "Nr. of Conversations",
      "Success",
      "Success Over Time",
      "Average",
      "Results"
    )
    df.show()
    df
      .write
      .option("header", value = true)
      .csv(s"$path${LocalDateTime.now()}")
  }
}
