package pragmatic_updating

import actors.Agent
import actors.general.FghSpeaker
import actors.personal.RmhSpeaker
import props.{Referent, Signal}

case object SpeakerFactory {

  def apply(agentType: String,
            name: String,
            conversationType: String,
            order: Int,
            signals: Vector[Signal],
            referents: Vector[Referent]):
  Agent = agentType match {
    case "fgh" => FghSpeaker(name, conversationType, order, signals, referents)
    case "rmh" => RmhSpeaker(name, conversationType, order, signals, referents)
  }
}

