package pragmatic_updating

import props.Lexicon

import scala.util.Random

object Metrics {

  def normalize(vector: Vector[Double]): Vector[Double] = vector.sum match {
    case 0 => vector
    case _ => vector map (p => p / vector.sum)
  }

  // total success
  // (the number of times the intention was equal to the inferred referent divided by the conversation length)
  def success(conv: Vector[Int]): Double =
    conv.count { e =>  e == 1 } / conv.size.toDouble

  // success over time
  // same thing but over time. This returns an array which contains the number of sucesses/index for each index
  def success_dt(conv: Vector[Int]): Vector[Double] = {

    // This puts at each index the number of successes
    val y = conv.scanLeft(0.0) { case (acc, el) => if (el == 1) acc + 1 else acc }.tail

    // This divides each value by the index
    y.indices.map(i => y(i) / (i + 1).toDouble) toVector
  }

  def avg(results: Vector[Vector[Int]]): Double =
    (results map { c => success(c) } sum) / results.size

  // Calculating the asymmetry between two lexicons
  def asymmetry(L1: Lexicon, L2: Lexicon): Double = {
    val totalSize = L1.L.size * L1.L(0).size
    val zipped = (L1.L, L2.L).zipped
    val difference = zipped flatMap { (l1, l2) => l1 diff l2 }
    val asymmetry = difference.size.toDouble / totalSize.toDouble
    asymmetry
  }

  // Uniform sample from most probable signals or referents (aux function) - returns index
  def argMax(dist: Vector[Double]): Int = {
    val zipped = dist.zipWithIndex
    val maxs = zipped filter { case (prob, _) => prob == dist.max }
    val randomMax = Random.shuffle(maxs).take(1)(0)._2
    randomMax
  }
}
