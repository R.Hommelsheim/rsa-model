package actors.general

import actors.Agent
import props.{Lexicon, Referent, Signal}

import scala.collection.mutable.ListBuffer

abstract class GeneralAgent(val name: String,
                            val conversationType: String,
                            val order: Int,
                            val signals: Vector[Signal],
                            val referents: Vector[Referent])
  extends Agent {

  override def setSigRef(sigRef: Vector[(Signal, Referent)]): Unit = agentSigRef = sigRef map identity

  protected def factorial(n: Int): Int = n match {
    case 0 | 1 => 1
    case n => n * factorial(n - 1)
  }

  // (n choose r) / L => Binomial distribution
  protected def binomial(lexicon: Lexicon): Double = {
    val n: Double = (signals.size * referents.size).toDouble
    val r: Double = (lexicon.L map {l => l.sum}).sum
    val denominator: Double = factorial((n - r).toInt) * factorial(r.toInt)
    val nChooseR: Double = factorial(n.toInt) / denominator.toDouble
    nChooseR / allLexicons.length.toDouble
  }

  val allLexicons: Vector[Lexicon] = getAllLexicons

  // Recursively creates all possible lexicons given row and col size (n, m)
  private def getAllLexicons: Vector[Lexicon] = {

    // Initialise empty lexicon of size (n, m)
    val lex = Array.fill(signals.length, referents.length)(0.0)

    // Recursive calls
    val zeros = put(lex, ListBuffer(lex), 0, 0, 0)
    val ones = put(lex, ListBuffer(), 0, 0, 1)

    // Return the list of all (immutable) lexicons
    (zeros ++ ones).map(_.map(_.toVector).toVector).map(l => Lexicon(l)).toVector
  }

  // Puts both a 0 and a 1 in each cell and calls itself recursively
  private def put(lex: Array[Array[Double]],
                  allLexicons: ListBuffer[Array[Array[Double]]],
                  row: Int,
                  col: Int,
                  putValue: Double): ListBuffer[Array[Array[Double]]] = {

    // Check whether we have all lexicons (base case)
    if (row >= lex.length)
      return allLexicons

    // Copy the given lexicon
    val newLex = lex.map(_.map(identity))

    // Put in the new value
    newLex(row)(col) = putValue

    // We only need to keep the ones where we put a 1 since we start with a matrix of 0s
    if (putValue == 1)
      allLexicons += newLex

    // Keep going while pointing to the correct cell
    if (col < lex(0).length - 1) {
      put(newLex, allLexicons, row, col + 1, 0)
      put(newLex, allLexicons, row, col + 1, 1)
    } else {
      put(newLex, allLexicons, row + 1, 0, 0)
      put(newLex, allLexicons, row + 1, 0, 1)
    }

    allLexicons
  }


  // Not used in general agents
  override var myIntention: Referent = _
  override var lexicon: Lexicon = _
  override def setLexicon(lex: Lexicon): Unit = lexicon = lex
  override def update(signal: Signal, referent: Referent): Vector[Vector[Double]] = Vector(Vector())
  override def equalPrior(): Vector[Vector[Double]] = Vector(Vector())

}
