package actors.general

import actors.Speaker
import pragmatic_updating.Metrics
import props.{Lexicon, Referent, Signal}

case class FghSpeaker(override val name: String,
                      override val conversationType: String,
                      override val order: Int,
                      override val signals: Vector[Signal],
                      override val referents: Vector[Referent])
  extends FghAgent(name, conversationType, order, signals, referents) with Speaker {

  def pSignal(signal: Signal, referent: Referent, data: Vector[(Signal, Referent)]): Double =
    Math.exp(
      alpha *
        Math.log(
          allLexicons.foldLeft(0.0)((sum, lexicon) =>
            sum +
              (posteriorS(data, lexicon) *
                listen(order - 1, signal, lexicon)(referent.id))))
        - cost(signal))

  // Posterior
  private def posteriorS(data: Vector[(Signal, Referent)], lexicon: Lexicon): Double =
    lexiconPrior(lexicon) *
      data.foldLeft(1.0) { case (prod, (s, r)) =>
        prod *
          listen(order - 1, s, lexicon)(r.id)
      }

  def sDist(intention: Referent): Vector[Double] =
    Metrics.normalize(signals map (signal =>
      pSignal(signal, intention, agentSigRef)))

  override def speak(intention: Referent): Signal =
    signals(Metrics.argMax(sDist(intention)))

}
