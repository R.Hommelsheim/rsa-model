package actors.general

import actors.Listener
import pragmatic_updating.Metrics
import props.{Lexicon, Referent, Signal}

case class FghListener(override val name: String,
                       override val conversationType: String,
                       override val order: Int,
                       override val signals: Vector[Signal],
                       override val referents: Vector[Referent])
  extends FghAgent(name, conversationType, order, signals, referents) with Listener {

  // Probability of a referent given a signal
  def pReferent(signal: Signal, referent: Referent, data: Vector[(Signal, Referent)]): Double =
    allLexicons.foldLeft(0d)((sum, lexicon) =>
      // accumulating sum
      sum +
        // posterior of lexicon given data
        (posteriorL(data, lexicon) *
          // probability of a referent given a lexicon
          listen(order, signal, lexicon)(referent.id)))

  // Posterior
  private def posteriorL(data: Vector[(Signal, Referent)], lexicon: Lexicon): Double =
    lexiconPrior(lexicon) *
      data.foldLeft(1d) { case (prod, (s, r)) =>
        prod * speak(order, r, lexicon)(s.id)
      }

  def lDist(signal: Signal): Vector[Double] =
    Metrics.normalize(referents map (referent => pReferent(signal, referent, agentSigRef)))

  // The inferred referent
  override def listen(signal: Signal): Referent =
    referents(Metrics.argMax(lDist(signal)))

}
