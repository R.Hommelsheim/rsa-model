package actors.general

import actors.{Listener, Speaker}
import pragmatic_updating.Metrics
import props.{Lexicon, Referent, Signal}

class FghAgent(name: String,
               conversationType: String,
               order: Int,
               signals: Vector[Signal],
               referents: Vector[Referent])
  extends GeneralAgent(name, conversationType, order, signals, referents) {

  // softmax optimality parameter
  protected val alpha = 1.0

  protected def speak(order: Int, referent: Referent, lexicon: Lexicon): Vector[Double] =
    Metrics.normalize(signals map {
      signal => Math.exp(alpha * Math.log(listen(order - 1, signal, lexicon)(referent.id)) - cost(signal))
    })

  // listener
  protected def listen(order: Int, signal: Signal, lexicon: Lexicon): Vector[Double] = order match {
    case 0 => Metrics.normalize(referents map { ref => lexicon.L(signal.id)(ref.id) * referentPrior(ref) })
    case _ =>
      Metrics.normalize(referents map { referent => referentPrior(referent) * speak(order, referent, lexicon)(signal.id) })
  }

  // prior of a referent which the listener uses. 1 over referents for now
  protected def referentPrior(referent: Referent): Double = 1.0 / referents.length.toDouble

  // Prior of a certain lexicon.
  protected def lexiconPrior(lexicon: Lexicon): Double = {
    binomial(lexicon)
  }

  // Cost of producing the signal. This could be measured by frequency or length but is for now 0.
  protected def cost(signal: Signal): Double = 0.0

  override def asSpeaker: Speaker = {
    val sp = FghSpeaker(name, conversationType, order, signals, referents)
    sp.agentSigRef = agentSigRef map identity
    sp
  }

  override def asListener: Listener = {
    val lis = FghListener(name, conversationType, order, signals, referents)
    lis.agentSigRef = agentSigRef map identity
    lis
  }

  override var agentSigRef: Vector[(Signal, Referent)] = Vector[(Signal, Referent)]()
  override var rPrior: Vector[Vector[Double]] = Vector(Vector())
}