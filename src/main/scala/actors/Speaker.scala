package actors

import props.{Referent, Signal}

trait Speaker extends Agent {
  def speak(intention: Referent): Signal
}