package actors

import props.{Lexicon, Referent, Signal}

import scala.util.Random

abstract class Agent {

  // for switching roles
  def asSpeaker: Speaker
  def asListener: Listener

  // Saving the data (signal, inferred referent)
  var agentSigRef: Vector[(Signal, Referent)]
  def setSigRef(data: Vector[(Signal, Referent)])

  // personal intention
  var myIntention: Referent

  // prior over referents
  var rPrior: Vector[Vector[Double]]
  def equalPrior(): Vector[Vector[Double]]

  // personal lexicon
  var lexicon: Lexicon
  def setLexicon(lexicon: Lexicon)

  def update(signal: Signal, referent: Referent): Vector[Vector[Double]]
}
