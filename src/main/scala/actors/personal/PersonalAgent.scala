package actors.personal

import actors.Agent
import props.{Lexicon, Referent, Signal}

abstract class PersonalAgent(val name: String,
                             val conversationType: String,
                             val order: Int,
                             val signals: Vector[Signal],
                             val referents: Vector[Referent])
  extends Agent {

}
