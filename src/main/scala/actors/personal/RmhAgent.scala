package actors.personal

import actors.{Listener, Speaker}
import pragmatic_updating.Metrics
import props.{Lexicon, Referent, Signal}

class RmhAgent(override val name: String,
               override val conversationType: String,
               override val order: Int,
               override val signals: Vector[Signal],
               override val referents: Vector[Referent])
  extends PersonalAgent(name, conversationType, order, signals, referents) {

  val alpha = 1.2

  override var lexicon: Lexicon = Lexicon.apply(signals, referents)

  var rPrior: Vector[Vector[Double]] = equalPrior()

  def equalPrior(): Vector[Vector[Double]] =
    Vector.fill(signals.size, referents.size)(1d) map { row => Metrics.normalize(row) }

  override def setLexicon(lex: Lexicon): Unit =
    lexicon = lex

  override var myIntention: Referent = _

  def setIntention(intention: Referent): Unit =
    myIntention = intention

  override var agentSigRef: Vector[(Signal, Referent)] = Vector[(Signal, Referent)]()

  def setSigRef(sigRef: Vector[(Signal, Referent)]): Unit =
    agentSigRef = sigRef map identity

  override def asSpeaker: Speaker = {
    val sp = RmhSpeaker(name, conversationType, order, signals, referents)
    copyStuff(sp)
    sp
  }

  override def asListener: Listener = {
    val lis = RmhListener(name, conversationType, order, signals, referents)
    copyStuff(lis)
    lis
  }

  def copyStuff(agent: RmhAgent): Unit = {
    agent.lexicon = lexicon.copy
    agent.agentSigRef = agentSigRef map identity
    agent.myIntention = myIntention
    agent.rPrior = rPrior
  }

  def listen(order: Int,
             prior: Vector[Vector[Double]] = rPrior)
  : Lexicon =
    order match {
      case 0 =>
        lexWithPriors(lexicon, prior).normalize
      case _ =>
        lexWithPriors(speak(order), prior).normalize
    }

  // Multiplies the priors into the lexicon
  def lexWithPriors(lex: Lexicon, prior: Vector[Vector[Double]]): Lexicon =
    Lexicon((lex.L zip prior) map { case (x, y) => (x zip y) map { case (m, n) => m * n } })

  def speak(order: Int): Lexicon = listen(order - 1).T.normalize.T

  override def update(signal: Signal, referent: Referent): Vector[Vector[Double]] = {
    if (referent.id == myIntention.id)
      update_aux(signal, 1.1)
    else
      update_aux(signal, 0.9)
  }

  protected def update_aux(signal: Signal, weight: Double): Vector[Vector[Double]] = {
    val sig1 = agentSigRef.last._1.id
    val sig2 = signal.id
    val priors = Lexicon.update(rPrior, sig1, myIntention.id, rPrior(sig1)(myIntention.id) * weight)
    Lexicon(Lexicon.update(priors, sig2, myIntention.id, priors(sig2)(myIntention.id) * weight)).normalize.L
  }
}
