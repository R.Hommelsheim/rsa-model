package actors.personal

import actors.Speaker
import pragmatic_updating.Metrics
import props.{Lexicon, Referent, Signal}

case class RmhSpeaker(override val name: String,
                      override val conversationType: String,
                      override val order: Int,
                      override val signals: Vector[Signal],
                      override val referents: Vector[Referent])
  extends RmhAgent(name, conversationType, order, signals, referents) with Speaker {


  override def speak(intention: Referent): Signal = {
    myIntention = intention

    // Calculate the probability lexicon
    var lex = speak(order)

    val signal = conversationType match {
      case "Monologue" =>
        signals(Metrics.argMax(lex.T.L(intention.id)))

      case "Dialogue" if agentSigRef.size % 2 == 0 =>
        lex = lexWithPriors(lex, rPrior)
        signals(Metrics.argMax(lex.T.L(intention.id)))

      case "Dialogue" =>
        //         If a signal has already been used,
        //         we need to find the second best signal (which hasn't been used yet)
        val last_sig: Int = agentSigRef.last._1.id
        val zipped = lex.T.L(intention.id).zipWithIndex
        val without_last: Vector[(Double, Int)] = zipped.filterNot { case (_, i) => i == last_sig }
        val sig = (without_last reduceLeft ((x, y) => if (x._1 < y._1) y else x))._2
        signals(sig)
    }

//    prints(intention, lex, signal)
    signal
  }

  def prints(intention: Referent,
             lex: Lexicon,
             signal: Signal): Unit = {

    println(s"Speaker with intention $intention")
    println(s"And lexicon with ambiguity ${lexicon.ambiguity()}: \n", lexicon)
    println(s"Speaker calculated the following table: \n", lex)
    println(s"Speaker says $signal")
  }

}
