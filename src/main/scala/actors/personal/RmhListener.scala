package actors.personal

import actors.Listener
import pragmatic_updating.Metrics
import props.{Lexicon, Referent, Signal}

case class RmhListener(override val name: String,
                       override val conversationType: String,
                       override val order: Int,
                       override val signals: Vector[Signal],
                       override val referents: Vector[Referent])
  extends RmhAgent(name, conversationType, order, signals, referents) with Listener {

  override def listen(signal: Signal): Referent = {

    // Compute the probabilities of the lexicon
    val lex = {
      if (conversationType == "Dialogue" && agentSigRef.size % 2 == 1) {
        // Assume that the other agent talks about my intended referent.
        val prior = Lexicon(rPrior.zipWithIndex map { case (sig, idx) =>
          sig.updated(myIntention.id, rPrior(idx)(myIntention.id) * alpha)
        }).normalize.L
        listen(order, prior)
      }
      else
        listen(order)
    }
    //    val lex = listen(order)

    // Get the interpreted referent
    val referent = referents(Metrics.argMax(lex.L(signal.id)))

    if (conversationType == "Dialogue" && agentSigRef.size % 2 == 1)
      rPrior = update(signal, referent)

    referent
  }

  def prints(lex: Lexicon, signal: Signal, referent: Referent): Unit = {
    println(s"Listener with lexicon with ambiguity ${lexicon.ambiguity()}: \n", lexicon)
    println(s"Listener calculated the following table: \n", lex)
    println(s"Listener heard $signal and came to the conclusion: $referent")
  }
}
