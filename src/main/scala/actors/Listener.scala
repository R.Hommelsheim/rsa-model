package actors

import props.{Referent, Signal}

trait Listener extends Agent {
  def listen(signal: Signal): Referent
}
